package cz.davidkocnar.meteoritetestapp.ui;

import android.Manifest;
import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.Utils;

import java.util.List;

import cz.davidkocnar.meteoritetestapp.R;
import cz.davidkocnar.meteoritetestapp.databinding.ActivityMeteoriteListBinding;
import cz.davidkocnar.meteoritetestapp.databinding.MeteoriteListContentBinding;
import cz.davidkocnar.meteoritetestapp.model.MeteoriteResponse;
import cz.davidkocnar.meteoritetestapp.viewmodel.Event;
import cz.davidkocnar.meteoritetestapp.viewmodel.MeteoritesViewModel;

/**
 * An activity containing list of meteorites and {@link CollapsingToolbarLayout}.
 * This activity contains also {@link MeteoriteDetailFragment} in two-pane mode (on tablets)
 * or only {@link RecyclerView} on handsets.
 */
public class MeteoriteListActivity extends AppCompatActivity {

	private static final int MY_PERMISSIONS_REQUEST_INTERNET = 0;
	private boolean mTwoPane;
	private ActivityMeteoriteListBinding binding;
	private MeteoritesViewModel viewModel;
	private Observable.OnPropertyChangedCallback onStateChanged = new Observable.OnPropertyChangedCallback() {
		@Override
		public void onPropertyChanged(Observable sender, int propertyId) {
			binding.statefulLayout.setState(viewModel.state.get());
		}
	};
	private MeteoritesRecyclerViewAdapter adapter;
	private int meteoritesCount = 0;
	boolean scrolled = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_meteorite_list);
		toolbarSetup();

		if (binding.meteoriteDetailContainer != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-w900dp).
			// If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;
		}

		ActivityCompat.requestPermissions(this,
				new String[]{Manifest.permission.INTERNET},
				MY_PERMISSIONS_REQUEST_INTERNET);

		Utils.init(this);
		viewModel = ViewModelProviders
				.of(this, new MeteoritesViewModel.Factory(this))
				.get(MeteoritesViewModel.class);
		viewModel.state.addOnPropertyChangedCallback(onStateChanged);
		viewModel.meteorites.observe(this, this::setAdapterData);
		viewModel.openDatepickerDialogEvent.observe(this, this::openDatepickerDialog);
		viewModel.init();

		binding.statefulLayout.setOfflineRetryOnClickListener(viewModel.offlineRetryListener);
		binding.meteoritesLayout.filterOption.setOnClickListener(view -> viewModel.filterBtnClicked());
	}

	private void toolbarSetup() {
		setSupportActionBar(binding.meteoritesLayout.toolbar);
		binding.meteoritesLayout.toolbar.setTitle("");
		binding.meteoritesLayout.toolbar.setTitleMarginTop(getStatusBarHeight());
		TypedValue tv = new TypedValue();
		int actionBarHeight = 0;
		if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
		}
		binding.meteoritesLayout.toolbar.setLayoutParams(
				new CollapsingToolbarLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
						actionBarHeight + getStatusBarHeight()));
		if (getSupportActionBar() != null) getSupportActionBar().setDisplayShowTitleEnabled(false);

		binding.meteoritesLayout.appBar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
			int totalScroll = appBarLayout.getTotalScrollRange();
			int currentScroll = totalScroll + verticalOffset;

			if ((currentScroll) == 0){
				if (!scrolled) {
					scrolled = true;
					refreshActivityTitle();
				}
			} else {
				if (scrolled) {
					scrolled = false;
					refreshActivityTitle();
				}
			}
		});
	}

	private void openDatepickerDialog(Event event) {
		if (event.isNotHandled()) {
			DatePickerDialog datePickerDialog = new DatePickerDialog(this, viewModel,
					event.getYear(), event.getMonth(), event.getDay());
			datePickerDialog.show();
		}
	}

	private void refreshActivityTitle() {
		if (scrolled) {
			binding.meteoritesLayout.toolbarLayout.setTitle(getString(R.string.title_x_meteorites, meteoritesCount));
		}
		else {
			binding.meteoritesLayout.toolbarLayout.setTitle(String.valueOf(meteoritesCount));
		}
		binding.meteoritesLayout.meteoritesSubtitle.setText(getString(R.string.meteorites_subtitle, viewModel.getLastDateString()));
	}

	private void setAdapterData(List<MeteoriteResponse> meteorites) {
		adapter = new MeteoritesRecyclerViewAdapter(this, meteorites, mTwoPane);
		runOnUiThread(() -> adapter.notifyDataSetChanged());
		binding.meteoritesLayout.recyclerView.setAdapter(adapter);
		if (meteorites != null) {
			meteoritesCount = meteorites.size();
			refreshActivityTitle();
		}
	}

	public int getStatusBarHeight() {
		Resources res = getResources();
		int result = 0;
		int resourceId = res.getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = res.getDimensionPixelSize(resourceId);
		}
		return result;
	}

	public static class MeteoritesRecyclerViewAdapter
			extends RecyclerView.Adapter<MeteoritesRecyclerViewAdapter.ViewHolder> {

		private final MeteoriteListActivity mParentActivity;
		private List<MeteoriteResponse> listData;
		private final boolean mTwoPane;
		private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				MeteoriteResponse item = (MeteoriteResponse) view.getTag();
				if (mTwoPane) {
					loadFragment(item);
				} else {
					Context context = view.getContext();
					Intent intent = new Intent(context, MeteoriteDetailActivity.class);
					intent.putExtra(MeteoriteDetailFragment.ARG_METEORITE_OBJ, item);

					context.startActivity(intent);
				}
			}
		};
		private LayoutInflater layoutInflater;

		MeteoritesRecyclerViewAdapter(MeteoriteListActivity parent,
									  List<MeteoriteResponse> items,
									  boolean twoPane) {
			listData = items;
			mParentActivity = parent;
			mTwoPane = twoPane;
		}

		@NonNull
		@Override
		public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			if (layoutInflater == null) {
				layoutInflater = LayoutInflater.from(parent.getContext());
			}
			MeteoriteListContentBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.meteorite_list_content, parent, false);
			return new ViewHolder(binding);
		}

		@Override
		public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
			holder.binding.setMeteorite(listData.get(position));
			holder.binding.getRoot().setOnClickListener(mOnClickListener);
			holder.binding.getRoot().setTag(listData.get(position));
		}

		@Override
		public int getItemCount() {
			return listData.size();
		}

		private void loadFragment(@NonNull MeteoriteResponse item) {
			Bundle arguments = new Bundle();
			arguments.putSerializable(MeteoriteDetailFragment.ARG_METEORITE_OBJ, item);
			MeteoriteDetailFragment fragment = new MeteoriteDetailFragment();
			fragment.setArguments(arguments);

			mParentActivity.getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.meteorite_detail_container, fragment)
					.disallowAddToBackStack()
					.commit();
		}

		public void setData(List<MeteoriteResponse> data) {
			this.listData = data;
		}

		class ViewHolder extends RecyclerView.ViewHolder {

			private final MeteoriteListContentBinding binding;

			ViewHolder(MeteoriteListContentBinding binding) {
				super(binding.getRoot());
				this.binding = binding;
			}
		}
	}
}
