package cz.davidkocnar.meteoritetestapp.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import cz.davidkocnar.meteoritetestapp.R;
import cz.davidkocnar.meteoritetestapp.model.MeteoriteResponse;

/**
 * A fragment representing a single Meteorite detail screen.
 * This fragment is either contained in a {@link MeteoriteListActivity}
 * in two-pane mode (on tablets) or a {@link MeteoriteDetailActivity}
 * on handsets.
 */
public class MeteoriteDetailFragment extends Fragment implements OnMapReadyCallback {

	public static final String ARG_METEORITE_OBJ = "ARG_METEORITE_OBJ";
	private MeteoriteResponse meteorite;
	private View rootView;

	public MeteoriteDetailFragment() {
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments() != null && getArguments().containsKey(ARG_METEORITE_OBJ)) {
			meteorite = (MeteoriteResponse) getArguments().getSerializable(ARG_METEORITE_OBJ);
		}
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.meteorite_detail, container, false);

		SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
		assert mapFragment != null;
		mapFragment.getMapAsync(this);
		return rootView;
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		if (meteorite != null && meteorite.getGeolocation() != null) {
			List<Double> coordinates = meteorite.getGeolocation().getCoordinates();

			LatLng position = new LatLng(coordinates.get(1), coordinates.get(0));
			MarkerOptions markerOptions = new MarkerOptions()
					.position(position)
					.title(meteorite.getName())
					.snippet(meteorite.getMassPrepared(rootView))
					.visible(true);

			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, 5);
			googleMap.addMarker(markerOptions).showInfoWindow();
			googleMap.moveCamera(cameraUpdate);
		}
		else {
			Snackbar.make(rootView, "Missing coordinates!", Snackbar.LENGTH_LONG).show();
		}
	}
}
