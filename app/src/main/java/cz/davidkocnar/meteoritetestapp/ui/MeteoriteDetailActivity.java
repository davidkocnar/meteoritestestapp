package cz.davidkocnar.meteoritetestapp.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import cz.davidkocnar.meteoritetestapp.R;
import cz.davidkocnar.meteoritetestapp.model.MeteoriteResponse;

/**
 * An activity containing {@link MeteoriteDetailFragment}.
 * This activity is used only for handsets.
 */
public class MeteoriteDetailActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_meteorite_detail);
		Toolbar toolbar = findViewById(R.id.detail_toolbar);
		setSupportActionBar(toolbar);

		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		MeteoriteResponse item = (MeteoriteResponse) getIntent().getSerializableExtra(MeteoriteDetailFragment.ARG_METEORITE_OBJ);
		setTitle(" ");

		if (savedInstanceState == null) {
			Bundle arguments = new Bundle();
			arguments.putSerializable(MeteoriteDetailFragment.ARG_METEORITE_OBJ, item);
			MeteoriteDetailFragment fragment = new MeteoriteDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.meteorite_detail_container, fragment)
					.commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
