package cz.davidkocnar.meteoritetestapp.data;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cz.davidkocnar.meteoritetestapp.R;
import cz.davidkocnar.meteoritetestapp.model.MeteoriteResponse;
import cz.davidkocnar.meteoritetestapp.rest.ApiInterface;
import cz.davidkocnar.meteoritetestapp.rest.RetrofitClient;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Repository class for remote communication.
 * It is retrieving data from REST API.
 */
public class MeteoritesRepository {

	private ApiInterface apiInterface;
	private Context context;
	public static final String datePattern = "yyyy-MM-dd";
	private static String order = "mass DESC";
	private static String token = "bLyoHY4mX5ev4rx8NZQWm8NbF";
	private static final int limit = 100000;
	private SimpleDateFormat format = new SimpleDateFormat(datePattern, Locale.ENGLISH);

	public MeteoritesRepository(Context context) {
		this.context = context.getApplicationContext();
		order = context.getString(R.string.rest_param_order);
	}

	public Observable<List<MeteoriteResponse>> requestData(int offset, Calendar calendar) {
		if (apiInterface == null) apiInterface = RetrofitClient.getClient(context).create(ApiInterface.class);
		String formattedDate = format.format(calendar.getTime());
		String where = context.getString(R.string.rest_param_where, formattedDate);
		Observable<List<MeteoriteResponse>> call = apiInterface.getMeteorites(order, where, limit, offset, token);

		return call.subscribeOn(Schedulers.newThread());
	}
}
