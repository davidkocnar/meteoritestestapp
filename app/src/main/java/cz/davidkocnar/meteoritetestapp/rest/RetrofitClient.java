package cz.davidkocnar.meteoritetestapp.rest;

import android.content.Context;

import com.blankj.utilcode.util.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by David on 20.06.2018.
 * Description:
 */
public class RetrofitClient {

	private static final String BASE_URL = "https://data.nasa.gov/resource/";
	private static Retrofit retrofit = null;
	private static final int oneDay = 86400;  // 1 day = 60*60*24 s
	private static final int oneWeek = 604800;  // 1 week = 60*60*24*7 s

	public static Retrofit getClient(Context context) {
		if (retrofit == null) {

			OkHttpClient okHttpClient = new OkHttpClient.Builder()
					.cache(new Cache(new File(context.getCacheDir(), "apiResponses"), 5 * 1024 * 1024)) // max 10 MB cache
					.addInterceptor(chain -> {
						Request request = chain.request();
						if (NetworkUtils.isConnected()) {  // When network is active - cache 1 day
							request = request.newBuilder().header("Cache-Control", "public, max-age=" + oneDay).build();
						} else {  // When network is inactive, offline mode - hold 1 week
							request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + oneWeek).build();
						}
						return chain.proceed(request);
					})
					.build();

			Gson gson = new GsonBuilder()
					.setLenient()
					.create();

			retrofit = new Retrofit.Builder()
					.baseUrl(BASE_URL)
					.client(okHttpClient)
					.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
					.addConverterFactory(GsonConverterFactory.create(gson))
					.build();
		}
		return retrofit;
	}
}
