package cz.davidkocnar.meteoritetestapp.rest;

import java.util.List;

import cz.davidkocnar.meteoritetestapp.model.MeteoriteResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by David on 20.06.2018.
 * Description:
 */
public interface ApiInterface {

	@GET("y77d-th95.json")
	Observable<List<MeteoriteResponse>> getMeteorites(@Query("$order") String order,
													  @Query("$where") String where,
													  @Query("$limit") int limit,
													  @Query("$offset") int offset,
													  @Query("$$app_token") String token);
}
