package cz.davidkocnar.meteoritetestapp.viewmodel;

import java.util.Calendar;

/**
 * Event class for triggered single event with inner data.
 */
public class Event {

	private Calendar calendar;
	private boolean hasBeenHandled = false;

	public Event(Calendar calendar) {
		this.calendar = calendar;
	}

	public boolean isNotHandled() {
		if (hasBeenHandled) {
			return false;
		}
		else {
			hasBeenHandled = true;
			return true;
		}
	}

	public int getYear() {
		return calendar.get(Calendar.YEAR);
	}

	public int getMonth() {
		return calendar.get(Calendar.MONTH);
	}

	public int getDay() {
		return calendar.get(Calendar.DAY_OF_MONTH);
	}
}
