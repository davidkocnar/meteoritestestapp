package cz.davidkocnar.meteoritetestapp.viewmodel;

import android.app.DatePickerDialog;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import cz.davidkocnar.meteoritetestapp.data.MeteoritesRepository;
import cz.davidkocnar.meteoritetestapp.model.MeteoriteResponse;
import cz.kinst.jakub.view.SimpleStatefulLayout;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * ViewModel class using {@link MeteoritesRepository} to retrieve remote data.
 * It has MutableLiveData fields ready for observers.
 */
public class MeteoritesViewModel extends ViewModel implements DatePickerDialog.OnDateSetListener {

	private final MeteoritesRepository repository;

	public MutableLiveData<List<MeteoriteResponse>> meteorites = new MutableLiveData<>();
	public MutableLiveData<Event> openDatepickerDialogEvent = new MutableLiveData<>();
	public ObservableField<String> state = new ObservableField<>();

	private Calendar dateForFiltering = Calendar.getInstance();

	@Inject
	MeteoritesViewModel(Context context) {
		this.repository = new MeteoritesRepository(context);

		SimpleDateFormat dateFormat = new SimpleDateFormat(MeteoritesRepository.datePattern, Locale.ENGLISH);
		try {
			dateForFiltering.setTime(dateFormat.parse("2011-01-01"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void init() {
		state.set(SimpleStatefulLayout.State.PROGRESS);
		if (this.meteorites.getValue() == null || this.meteorites.getValue().size() == 0) {
			meteorites.postValue(null);
			Observable<List<MeteoriteResponse>> observable = repository.requestData(0, dateForFiltering);
			observable.observeOn(AndroidSchedulers.mainThread())
					.subscribe(remoteDataObserver);

		}
		else if (this.meteorites.getValue() != null) {
			useLoadedData();
		}
	}

	private Observer<List<MeteoriteResponse>> remoteDataObserver = new Observer<List<MeteoriteResponse>>() {
		@Override
		public void onError(Throwable e) {
			Log.w("MeteoritesViewModel", "Network error: " + e.getMessage());
			setRequestError();
		}

		@Override
		public void onComplete() {}

		@Override
		public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {}

		@Override
		public void onNext(List<MeteoriteResponse> response) {
			Log.d("MeteoritesViewModel", "Data received. size:" + response.size());
			if (response.isEmpty() || response.get(0) == null) {
				setEmpty();
			} else {
				setData(response);
			}
		}
	};

	private void setEmpty() {
		state.set(SimpleStatefulLayout.State.EMPTY);
	}

	private void setRequestError() {
		state.set(SimpleStatefulLayout.State.OFFLINE);
	}

	public void setData(List<MeteoriteResponse> data) {
		meteorites.postValue(data);
		state.set(SimpleStatefulLayout.State.CONTENT);
	}

	public String getLastDateString() {
		SimpleDateFormat format = new SimpleDateFormat(MeteoritesRepository.datePattern, Locale.ENGLISH);
		return format.format(dateForFiltering.getTime());
	}

	private void useLoadedData() {
		this.state.set(SimpleStatefulLayout.State.CONTENT);
		this.state.notifyChange();
	}

	@Override
	public void onDateSet(DatePicker datePicker, int year, int month, int day) {
		dateForFiltering.set(year, month, day);
		meteorites.setValue(null);
		init();
	}

	public void filterBtnClicked() {
		openDatepickerDialogEvent.setValue(new Event(dateForFiltering));
	}

	public View.OnClickListener offlineRetryListener = v -> init();

	/*
	 * Factory class of ViewModel
	 */
	public static class Factory implements ViewModelProvider.Factory {

		private final Context context;

		public Factory(Context context) {
			this.context = context;
		}

		@SuppressWarnings("unchecked")
		@NonNull
		@Override
		public MeteoritesViewModel create(@NonNull Class modelClass) {
			return new MeteoritesViewModel(context);
		}
	}

}
