package cz.davidkocnar.meteoritetestapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by David on 20.06.2018.
 * Description:
 */
public class Geolocation implements Serializable {

	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("coordinates")
	@Expose
	private List<Double> coordinates = null;
	private final static long serialVersionUID = -9087050683056914393L;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Double> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<Double> coordinates) {
		this.coordinates = coordinates;
	}
}
