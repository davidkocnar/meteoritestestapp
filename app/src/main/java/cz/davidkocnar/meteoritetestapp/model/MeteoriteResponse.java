package cz.davidkocnar.meteoritetestapp.model;

import android.databinding.BaseObservable;
import android.util.Log;
import android.view.View;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.davidkocnar.meteoritetestapp.R;

/**
 * Created by David on 20.06.2018.
 * Description:
 */
public class MeteoriteResponse extends BaseObservable implements Serializable {

	@SerializedName("fall")
	@Expose
	private String fall;
	@SerializedName("geolocation")
	@Expose
	private Geolocation geolocation;
	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("mass")
	@Expose
	private String mass;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("nametype")
	@Expose
	private String nametype;
	@SerializedName("recclass")
	@Expose
	private String recclass;
	@SerializedName("reclat")
	@Expose
	private String reclat;
	@SerializedName("reclong")
	@Expose
	private String reclong;
	@SerializedName("year")
	@Expose
	private String year;
	@SerializedName(":@computed_region_cbhk_fwbd")
	@Expose
	private String computedRegionCbhkFwbd;
	@SerializedName(":@computed_region_nnqa_25f4")
	@Expose
	private String computedRegionNnqa25f4;
	private final static long serialVersionUID = 8280046728227367531L;

	public String getFall() {
		return fall;
	}

	public void setFall(String fall) {
		this.fall = fall;
	}

	public Geolocation getGeolocation() {
		return geolocation;
	}

	public void setGeolocation(Geolocation geolocation) {
		this.geolocation = geolocation;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMass() {
		return mass;
	}

	public String getMassPrepared(View view) {
		double massNum = Double.parseDouble(this.mass);
		if (massNum >= 10000) {
			double massKg = massNum / 1000;
			String resultMass = String.format(Locale.ENGLISH, "%,.0f", massKg).replace(",", " ");
			return view.getContext().getResources().getString(R.string.mass_string_kg, resultMass);
		}
		else {
			String resultMass = String.format(Locale.ENGLISH, "%,.0f", massNum).replace(",", " ");
			return view.getContext().getResources().getString(R.string.mass_string_g, resultMass);
		}
	}

	public void setMass(String mass) {
		this.mass = mass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNametype() {
		return nametype;
	}

	public void setNametype(String nametype) {
		this.nametype = nametype;
	}

	public String getRecclass() {
		return recclass;
	}

	public void setRecclass(String recclass) {
		this.recclass = recclass;
	}

	public String getReclat() {
		return reclat;
	}

	public void setReclat(String reclat) {
		this.reclat = reclat;
	}

	public String getReclong() {
		return reclong;
	}

	public void setReclong(String reclong) {
		this.reclong = reclong;
	}

	public String getYear() {
		return year;
	}

	public String getYearPrepared(View view) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
		try {
			if (year==null) return "";
			Date yourDate = parser.parse(year);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(yourDate);
			return view.getContext().getResources().getString(R.string.year_string, calendar.get(Calendar.YEAR));
		} catch (ParseException e) {
			Log.w("MeteoriteResponse", "Error while parsing year string! year=" + year);
			return "";
		}
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getComputedRegionCbhkFwbd() {
		return computedRegionCbhkFwbd;
	}

	public void setComputedRegionCbhkFwbd(String computedRegionCbhkFwbd) {
		this.computedRegionCbhkFwbd = computedRegionCbhkFwbd;
	}

	public String getComputedRegionNnqa25f4() {
		return computedRegionNnqa25f4;
	}

	public void setComputedRegionNnqa25f4(String computedRegionNnqa25f4) {
		this.computedRegionNnqa25f4 = computedRegionNnqa25f4;
	}

}
