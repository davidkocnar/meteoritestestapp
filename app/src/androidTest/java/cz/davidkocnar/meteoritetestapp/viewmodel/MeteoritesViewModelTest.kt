package cz.davidkocnar.meteoritetestapp.viewmodel

import android.content.Context
import android.support.test.InstrumentationRegistry
import com.blankj.utilcode.util.Utils
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations

/**
 * Created by David on 03.07.2018.
 * Description:
 */
class MeteoritesViewModelTest {

    private val context: Context by lazy {
        InstrumentationRegistry.getTargetContext()
    }

    private val viewModel: MeteoritesViewModel by lazy {
        MeteoritesViewModel(context)
    }

    @Before
    fun setupRegisterViewModel() {
        MockitoAnnotations.initMocks(this)
        Utils.init(context.applicationContext)
    }

    @Test
    fun init() {
        viewModel.init()
        assertEquals("2011-01-01", viewModel.lastDateString)
    }

    @Test
    fun test2() {
        assertNotNull(context)
    }
}
