## Meteorites Test App

This Android app contains a list of meteorites, a filtering option and a detail screen with a map. On tablets it has a different layout.

--------

Tech stack:

- Android SDK
- Java 8 > Kotlin 
- Android Architecture Components (Jetpack)
- RxJava 2
- Retrofit 2
- SimpleStatefulLayout
- Google Play Services - Maps API

--------

Minimal SDK (API) version: 16

Target SDK (API) version: 27
